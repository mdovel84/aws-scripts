#!/local/bin/env python
import boto3
import yaml
import pprint
import os
import time





pr = pprint.PrettyPrinter(indent=4)

with open('server.yml','r') as fh:
    data = yaml.safe_load(fh)

print()
print('=== Yaml Data ===')
pr.pprint(data)

# CreateInstances Parameters 
cp = {}

# Pick an Image Id depending on the ami_type specified
ami_type = data['server']['ami_type']
architecture = data['server']['architecture']

if ami_type == 'amzn2':
    if architecture == 'x86_64':
        cp['ImageId'] = 'ami-0b614a5d911900a9b'
    if architecture == 'arm64':
        cp['ImageId'] = 'ami-0a1c7c7935530ecbf'
elif ami_type == 'ubuntu':
    if architecture == 'x86_64':
        cp['ImageId'] = 'ami-0fb653ca2d3203ac1'
    if architecture == 'arm64':
        cp['ImageId'] = 'ami-02af65b2d1ebdfafc'
else:
    print(f'ami type {ami_type} not supported...')

 
# Instance Type
cp['InstanceType'] = data['server']['instance_type']


# Setup Min and Max Count
cp['MinCount'] = data['server']['min_count']
cp['MaxCount'] = data['server']['max_count']


# Setup Block Device Mappings based on root_device_type
# Validate the root device name for the image type
client = boto3.client('ec2')
ec2 = boto3.resource('ec2')


# ===============
#  KeyPairs
# ===============

# os.system('openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem')

KEYNAME = 'ec2-keypair'

# create keypairs
with open(KEYNAME+'.pem', 'w') as kpfile:
    try:
        key_pair = ec2.create_key_pair(KeyName=KEYNAME)
        kpfile.write(str(key_pair.key_material))
        os.system(f'chmod 600 {KEYNAME}.pem')
    except Exception as e:
        print('Could not create key-pair.')
        print(type(e).__name__,e)


cp['UserData'] = "#!/bin/bash\n"

image_id = cp['ImageId']
response = client.describe_images(ImageIds=[image_id])
root_device_name = response['Images'][0]['RootDeviceName']
if data['server']['volumes'][0]['device'] != root_device_name:
    print()
    print(f"=== Error: Root Device {data['server']['volumes'][0]['device']} does not match device name {root_device_name} for ami image type {ami_type}. Update your yaml ===")
    quit()

if data['server']['root_device_type'] == 'ebs':
    # Generate a BlockDeviceMappings for each device
    cp['BlockDeviceMappings'] = []

    eph_num = 0
    for vol in data['server']['volumes']:

         # Mount the Drive for any drive other than the root partition
         if eph_num > 0:
             cp['UserData'] += f"mkfs -t {vol['type']} {vol['device']}\n"
             cp['UserData'] += f"mkdir /mnt/drive{eph_num}\n"
             cp['UserData'] += f"mount {vol['device']} /mnt/drive{eph_num}\n"

         bdp = {
            'DeviceName': vol['device'],
            'VirtualName': f'ephemeral{eph_num}',
            'Ebs': {
                'DeleteOnTermination': True,
                'VolumeSize': vol['size_gb'],
                'VolumeType': 'standard',
            },
         }
         cp['BlockDeviceMappings'].append(bdp)
         eph_num += 1
        


else:
    print('Root Block Device Type {root_device_type}')


ssh_cmds = []

# Create users based on the input data
iam = boto3.client('iam')
print()
print(f'=== Creating Users ===')
for user in data['server']['users']:

    # # Generate an ID RSA for that user
    # create keypairs
    try:
        os.system(f"rm -rf {user['login']}.key*")
        os.system(f"ssh-keygen -f {user['login']}.key -C {user['login']}@aws-machine")
        os.system(f"chmod 600 {user['login']}.key*")
        with open(f"{user['login']}.key.pub") as f:
            user['ssh_key'] = f.read().replace('\n','')
    except Exception as e:
        print('Could not create key-pair.')
        print(type(e).__name__,e)

    # Add user Data
    cp['UserData'] += f"""
# Adding user {user['login']}
adduser {user['login']}
mkdir /home/{user['login']}/.ssh
chmod 700 /home/{user['login']}/.ssh
touch /home/{user['login']}/.ssh/authorized_keys
chmod 600 /home/{user['login']}/.ssh/authorized_keys
echo {user['ssh_key']} > /home/{user['login']}/.ssh/authorized_keys
chown -R {user['login']} /home/{user['login']}/.ssh
chgrp -R {user['login']} /home/{user['login']}/.ssh
    """
    ud = {
        'UserName' : user['login']
    }
    response = iam.get_user(**ud)    # Does the user already exist?
    if response.get('User',{'UserName':False})['UserName']:
        print(f"Username {user['login']} already exists, not creating a new one: {response}")
    else:
        response = iam.create_user(**ud)
        print(response)

    
    

    # key = RSA.gen_key(1024, 65337)
    # key.save_key(f"/tmp/id_rsa.{user['login']}")
    # # Run ssh keygen to 

      
cp['KeyName'] = KEYNAME

# Add the First Users ARN to the Instance Arn Profile
# ud = {
#     'UserName' : data['server']['users'][0]['login']
# }
# response = iam.get_user(**ud)
# iip = {
#     'Name' : response['User']['Arn'],
# }
# cp['IamInstanceProfile'] = iip



print()
print(f'=== Create Instances Parameters ===')
pr.pprint(cp)


fetch_client = boto3.client('ec2', region_name = 'us-east-2')


# Create an ec2 instance
instances = ec2.create_instances(**cp)

# Wait for instance running
instances[0].wait_until_running()
instances[0].reload()
public_ip = instances[0].public_ip_address
print('running...')
print('Authorizing SSH inbound')

try:
    sgs = instances[0].security_groups
    print(sgs)
    sgId = sgs[0]['GroupId']
    resp = client.authorize_security_group_ingress(
            GroupId=sgId,
            IpPermissions=[
                {'IpProtocol': 'tcp',
                 'FromPort': 22,
                 'ToPort': 22,
                 'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
        ])
except Exception as e:
    print(e)

for user in data['server']['users']:
    print(f"ssh -i {user['login']}.key {user['login']}@{public_ip}")

