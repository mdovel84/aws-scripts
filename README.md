# AWS Scripts


This repository contains scripts that can be used to setup and takedown aws machines using boto3 python library

* aws-create - create an aws machine with users according to a yaml file
* aws-term - fully shut down all aws instances
* aws-down - shutdown any running aws instances
* aws-up - Startup aws instances that are stopped

