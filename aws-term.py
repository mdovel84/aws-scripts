import boto3

ec2 = boto3.resource('ec2')

for i in ec2.instances.all():
    print(f"{i} : {i.state['Name']}")
    if i.state['Name'] != 'terminated':
        print(f'Terminating Instance {i}')
        i.terminate()


